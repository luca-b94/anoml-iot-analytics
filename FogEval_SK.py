import pandas as pd
from scipy import stats
import pickle
import time
import sys
import numpy as np
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.metrics import auc, confusion_matrix, accuracy_score, recall_score, precision_score, f1_score, roc_curve
#Usage: PiEval_SK_AllValues.py <ModelsNames.csv> <ModelsDirectory>
try:
    print("Looking for models listed in", sys.argv[1], "file from ", sys.argv[2], "directory", "with dataset file:", sys.argv[3], "Logs will be saved in:", sys.argv[4])
except:
    print ("Usage: FogEval_SK.py <ModelsNames.csv> <ModelsDirectory> <AttackDataSet> <LogFileName>")
    quit()
def PA_RNN(DF, PreFix):
    PreFix = PreFix+"Anomaly"
    fpr, tpr, thresholds=roc_curve(DF["AttackLable"], DF[PreFix])
    roc_auc = auc(fpr, tpr)
    CM = confusion_matrix(DF["AttackLable"], DF[PreFix])
    Acc = accuracy_score(DF["AttackLable"], DF[PreFix])
    RC = recall_score(DF["AttackLable"], DF[PreFix], zero_division=1)
    P = precision_score(DF["AttackLable"], DF[PreFix], zero_division=1)
    F1 = f1_score(DF["AttackLable"], DF[PreFix])
    return roc_auc, CM, Acc, RC, P, F1
Analysis = {}
Analysis["Kurtosis"]= stats.kurtosis
Analysis["Skew"]= stats.skew
Analysis["MAD"]= stats.median_abs_deviation
Analysis["Average"]= np.average
Analysis["StDev"]= np.std
def WriteLog(Data, FileName):
    File = open(FileName, "a")
    File.write(Data+"\r\n")
    File.close()
min_max_scaler = MinMaxScaler()
AlgoPaths = open(sys.argv[1], "r")
ModelBase = sys.argv[2].rstrip("/")+"/"
for AlgoLine in AlgoPaths.readlines():
    ModelStartTime = time.time()
    AlgoLine = AlgoLine.split(",")
    try:
        DataFile = pd.read_csv(sys.argv[3])
    except:
        print (sys.argv[3], "file not found")
    DataFile = DataFile.rename(columns={'Attack LABLE (1:No Attack, -1:Attack)': 'AttackLable'})
    Readings = DataFile.columns
    Readings = Readings[1:-1]
    DataFile[Readings] = DataFile[Readings].fillna(0)
    AlgoPath = AlgoLine[0].replace("\n", "")
    PredictionTime = []
    Anomaly=[]
    try:
        model = pickle.load(open(ModelBase+AlgoPath, 'rb'))
    except:
        print (ModelBase+AlgoPath, "Model not found")
        continue
    AlgoDetails = AlgoPath.split("-")
    Scaler = AlgoDetails[1]
    if (Scaler == "MM"):
        ScalerStartTime = time.time()
        TestDataFile = pd.DataFrame(min_max_scaler.fit_transform(pd.DataFrame(DataFile[Readings])),columns=[Readings])
        ScalerEndTime = time.time()
        ScaleTime = ScalerEndTime - ScalerStartTime
    elif (Scaler == "SS"):
        ScalerStartTime = time.time()
        TestDataFile = pd.DataFrame(StandardScaler().fit_transform(pd.DataFrame(DataFile[Readings])),columns=[Readings])
        ScalerEndTime = time.time()
        ScaleTime = ScalerEndTime - ScalerStartTime
    elif (Scaler == "Kurtosis" or Scaler == "Average" or Scaler == "MAD" or Scaler == "Skew" or Scaler == "StDev"):
        norz = []
        ScalerStartTime = time.time()
        for ns in DataFile[Readings].to_numpy():
            norz.append(Analysis[Scaler](ns))
        ScalerEndTime = time.time()
        ScaleTime = ScalerEndTime - ScalerStartTime
        TestDataFile = pd.DataFrame(norz, columns=['value'])
        Readings = TestDataFile.columns
    Variants = len(Readings)
    infer_start_time = time.time()
    Anomaly = model.predict(TestDataFile)
    infer_end_time = time.time()
    infer_time = 1000*(infer_end_time - infer_start_time)
    DataFile[AlgoPath+"Anomaly"] = pd.Series(Anomaly)
    roc_auc, CM, Acc, RC, P, F1 = PA_RNN(DataFile, AlgoPath)
    CM = '-'.join(str(item) for innerlist in CM.tolist() for item in innerlist)
    PT = infer_time
    ModelEndTime = time.time()
    ModelTime = ModelEndTime - ModelStartTime
    Log = AlgoPath+","+str(PT)+","+str(roc_auc)+","+str(Acc)+","+str(RC)+","+str(P)+","+str(F1)+","+CM+","+str(ModelTime)+","+str(ScaleTime)+","+str(DataFile.shape[0])
    WriteLog(Log, sys.argv[4])
    DataFile.to_csv(AlgoLine[0].rstrip("\n")+"_SKL_Report.csv", index=None)
    print ("Detailed Report of", AlgoLine[0].rstrip("\n"), "is saved in", AlgoLine[0].rstrip("\n")+"_SKL_Report.csv")
