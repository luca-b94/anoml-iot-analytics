import os
from numpy.core.numeric import Inf
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '5' #Ignore any possible warnings
import pandas as pd
import tflite_runtime.interpreter as tf
import sys
from scipy import stats
import time
import numpy as np
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.metrics import auc, confusion_matrix, accuracy_score, recall_score, precision_score, f1_score, roc_curve
#Usage: PiEval_SK_AllValues.py <ModelsNames.csv> <ModelsDirectory>
try:
    print("Looking for models listed in", sys.argv[1], "file from ", sys.argv[2], "directory", "with dataset file:", sys.argv[3], "Logs will be saved in:", sys.argv[4])
except:
    print ("Usage: FogEval_TF <ModelsNames.csv> <ModelsDirectory> <AttackDataSet> <LogFileName>")
    quit()
def PA_RNN(DF, PreFix):
    PreFix = PreFix+"Anomaly"
    fpr, tpr, thresholds=roc_curve(DF["AttackLable"], DF[PreFix])
    roc_auc = auc(fpr, tpr)
    CM = confusion_matrix(DF["AttackLable"], DF[PreFix])
    Acc = accuracy_score(DF["AttackLable"], DF[PreFix])
    RC = recall_score(DF["AttackLable"], DF[PreFix], zero_division=1)
    P = precision_score(DF["AttackLable"], DF[PreFix], zero_division=1)
    F1 = f1_score(DF["AttackLable"], DF[PreFix])
    return roc_auc, CM, Acc, RC, P, F1
Analysis = {}
Analysis["Kurtosis"]= stats.kurtosis
Analysis["Skew"]= stats.skew
Analysis["MAD"]= stats.median_abs_deviation
Analysis["Average"]= np.average
Analysis["StDev"]= np.std
def WriteLog(Data, FileName):
    File = open(FileName, "a")
    File.write(Data+"\r\n")
    File.close()
def GenerateMVTSData(DF, DPs, TIME_STEPS):
    data = DF[DPs].to_numpy()
    from sklearn.model_selection import train_test_split
    rows = len(DF)
    Xs = []
    Ys = []
    for i in range(0, (rows - TIME_STEPS)):
        Xs.append(data[i:i+TIME_STEPS])
        Ys.append(data[i:i+TIME_STEPS])
    tr_x, ts_x, tr_y, ts_y = [np.array(x) for x in train_test_split(Xs, Ys, test_size=0.01)]
    assert tr_x.shape[2] == ts_x.shape[2] == (data.shape[1] if (type(data) == np.ndarray) else len(data))
    return  (tr_x.shape[2], tr_x, tr_y, ts_x, ts_y)
TIME_STEPS=30
RowLimit = 50000
min_max_scaler = MinMaxScaler()
AlgoPaths = open(sys.argv[1], "r")
ModelBase = sys.argv[2].rstrip("/")+"/"
for AlgoLine in AlgoPaths.readlines():
    ModelStartTime = time.time()
    AlgoLine = AlgoLine.split(",")
    try:
        DataFile = pd.read_csv(sys.argv[3])
    except:
        print (sys.argv[3], "file not found")
    DataFile = DataFile.rename(columns={'Attack LABLE (1:No Attack, -1:Attack)': 'AttackLable'})
    Readings = DataFile.columns
    Readings = Readings[1:-1]
    DataFile[Readings] = DataFile[Readings].fillna(0)
    AlgoPath = ModelBase+AlgoLine[0]
    AlgoThreshold=float(AlgoLine[1])
    AlgoLoss = []
    PredictTime = []
    Anomaly=[]
    interpreter = tf.Interpreter(model_path=AlgoPath+".tfl")
    interpreter.allocate_tensors()
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()
    AlgoDetails = AlgoPath.split("-")
    Scaler = AlgoDetails[1]
    if (Scaler == "MM"):
        ScalerStartTime = time.time()
        TestDataFile = pd.DataFrame(min_max_scaler.fit_transform(pd.DataFrame(DataFile[Readings])),columns=[Readings])
        ScalerEndTime = time.time()
        ScaleTime = ScalerEndTime - ScalerStartTime
    elif (Scaler == "SS"):
        ScalerStartTime = time.time()
        TestDataFile = pd.DataFrame(StandardScaler().fit_transform(pd.DataFrame(DataFile[Readings])),columns=[Readings])
        ScalerEndTime = time.time()
        ScaleTime = ScalerEndTime - ScalerStartTime
    elif (Scaler=="NS"):
        TestDataFile = DataFile[Readings]
        ScaleTime=0.0
    elif (Scaler == "Kurtosis" or Scaler == "Average" or Scaler == "MAD" or Scaler == "Skew" or Scaler == "StDev"):
        norz = []
        Variants = 1
        ScalerStartTime = time.time()
        for ns in DataFile[Readings].to_numpy():
            norz.append(Analysis[Scaler](ns))
        ScalerEndTime = time.time()
        ScaleTime = ScalerEndTime - ScalerStartTime
        TestDataFile = pd.DataFrame(norz, columns=['value'])
        Readings = TestDataFile.columns
    Variants = len(Readings)
    cp, pp = 0, 0
    for i in range(TestDataFile.shape[0] - TIME_STEPS):
        iter_st = time.time()
        InferValue = TestDataFile[Readings].iloc[i:i+TIME_STEPS]
        st = time.time()
        InferValue = InferValue.to_numpy()
        InferValue = InferValue.astype(np.float32)
        InferValue = InferValue.reshape((input_details[0]["shape"]), order='F')
        NPTime = 1000*(time.time() - st)
        infer_start_time = time.time()
        interpreter.set_tensor(input_details[0]['index'], InferValue)
        interpreter.invoke()
        infer_end_time = time.time()
        infer_time = 1000*(infer_end_time - infer_start_time)
        PredictTime.append(infer_time)
        st = time.time()
        result = interpreter.get_tensor(output_details[0]["index"])
        result = result.reshape((result.shape[1], result.shape[2]), order='F')
        InferValue = InferValue.reshape((InferValue.shape[1], InferValue.shape[2]), order='F')
        et = time.time()
        PostTime = (et - st) * 1000
        st = time.time()
        Loss = np.mean(np.mean(np.abs(result - InferValue), axis=1))
        et = time.time()
        LossTime = (et - st) * 1000
        AlgoLoss.append(Loss)
        if (Loss > AlgoThreshold):
            Anomaly.append(-1)
        elif(Loss <= AlgoThreshold):
            Anomaly.append(1)
        cp = int((i/DataFile.shape[0])*100)
        if (cp>pp):
            print (AlgoPath, cp, "% Completed")
            pp = cp
        iter_et = time.time()
        iter_time = (iter_et - iter_st)*1000
    DataFile = DataFile[:len(Anomaly)]
    DataFile[AlgoLine[0]+"Prob"] = AlgoLoss
    DataFile[AlgoLine[0]+"Anomaly"] = pd.Series(Anomaly)
    roc_auc, CM, Acc, RC, P, F1 = PA_RNN(DataFile, AlgoLine[0])
    CM = '-'.join(str(item) for innerlist in CM.tolist() for item in innerlist)
    PT = sum(PredictTime) / i
    ModelEndTime = time.time()
    ModelTime = ModelEndTime - ModelStartTime
    Log = AlgoPath+","+str(PT)+","+str(roc_auc)+","+str(Acc)+","+str(RC)+","+str(P)+","+str(F1)+","+CM+","+str(ModelTime)+","+str(ScaleTime)+","+str(DataFile.shape[0])
    WriteLog(Log, sys.argv[4])
    DataFile.to_csv(AlgoLine[0].rstrip("\n")+"_TFL_Report.csv", index=None)
    print ("Detailed Report of", AlgoLine[0].rstrip("\n"), "is saved in", AlgoLine[0].rstrip("\n")+"_TFL_Report.csv")