#The AnoML-IoT Pipeline

**Tools/Components utilized during the AnoML-IoT evalution:**


* Grove sensors:
  *  [Grove - Temperature&Humidity Sensor (High-Accuracy &Mini) v1.0](https://wiki.seeedstudio.com/Grove-TemptureAndHumidity_Sensor-High-Accuracy_AndMini-v1.0/)   
  *  [Grove - Light Sensor](https://wiki.seeedstudio.com/Grove-Light_Sensor)
  *  [Grove - Loudness Sensor](https://wiki.seeedstudio.com/Grove-Loudness_Sensor/) 
  *  [Grove - Air Quality Sensor v1.3](https://wiki.seeedstudio.com/Grove-Air_Quality_Sensor_v1.3/)
  *  [Digi XBee 3 Zigbee 3 RF Module](https://www.digi.com/products/embedded-systems/digi-xbee/rf-modules/2-4-ghz-rf-modules/xbee3-zigbee-3)
  *  [Grove - UART Wifi V2](https://wiki.seeedstudio.com/Grove-UART_Wifi_V2/)
* Microcontollers:
  * [Arduino Nano 33 BLE Sense](https://www.raspberrypi.org/products/raspberry-pi-pico/specifications/)
  * [Raspberry Pi Pico](https://www.raspberrypi.org/products/raspberry-pi-pico/specifications/)
  * [Arduino Nano RP2040 Connect](https://docs.arduino.cc/hardware/nano-rp2040-connect)
* Shields:
  * [Grove - Bee Socket](https://wiki.seeedstudio.com/Grove-Bee_Socket/)
  * [Grove Shield for Pi Pico V1.0](https://wiki.seeedstudio.com/Grove-Bee_Socket)
  * [Arduino Tiny Machine Learning Shield](https://store.arduino.cc/tiny-machine-learning-kit)
* Single-board computer
  * [Raspberry Pi 4 Model B](https://www.raspberrypi.org/products/raspberry-pi-4-model-b/)
* Software tools:
  * [Putty](https://www.putty.org/)
  * [Raspberry Pi Imager](https://www.raspberrypi.org/software/)
  * [Node-RED](https://nodered.org/)
  * [Arduino IDE 1.8.15](https://www.arduino.cc/en/software)
  * [Visual Studio Code](https://code.visualstudio.com/)

How to generate a dataset via Grove sensors and Arduino is explained [here.](https://github.com/hkayann/grove-dataset-generation)

The dataset is available at [Kaggle](https://www.kaggle.com/hkayan/anomliot).

The link to the [Edge to Cloud Generator](https://eccg.herokuapp.com/the_eccg).

The source code of the ECCG is available [here](https://gitlab.com/IOTGarage/eccg).

The source code of the `node-red-contrib-ble-sense` is available [here](https://gitlab.com/IOTGarage/node-red-anoml-iot).
