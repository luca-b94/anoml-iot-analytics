/* LIBS */
#include <TH02_dev.h>
#include "Air_Quality_Sensor.h"

/* VARIABLES */ 
AirQualitySensor airSensor(A0);
unsigned long frequency = 10000; // 4 bytes, miliseconds
unsigned long startTime;
float temperature;
float humidity;
int airQuality;
int lightDensity;
int soundLevel;
char sensorArray[10];
char dataArray[30];

void setup() 
{
    Serial.begin(9600);
    delay(150); // Power up delay.
    TH02.begin();
    delay(100); // Let TH02 begin.
    airSensor.init();
    startTime = millis();
}

void loop() 
{
    dataArray[0] = 0; 
    if ((millis()-startTime) > frequency)
    {
        /*Get Data From Sensors*/
        temperature = TH02.ReadTemperature();
        humidity = TH02.ReadHumidity();
        airQuality = airSensor.getValue();
        lightDensity = analogRead(A1);
        soundLevel = analogRead(A2); 

        /*Convert and concatenate*/
        itoa(startTime, sensorArray, 10);
        strcat(dataArray, sensorArray);
        strcat(dataArray, ",");
        dtostrf(temperature, 5, 2, sensorArray);
        strcat(dataArray, sensorArray);
        strcat(dataArray, ",");
        dtostrf(humidity, 5, 2, sensorArray);
        strcat(dataArray, sensorArray);
        strcat(dataArray, ",");
        itoa(airQuality, sensorArray, 10);
        strcat(dataArray, sensorArray);
        strcat(dataArray, ",");
        itoa(lightDensity, sensorArray, 10);
        strcat(dataArray, sensorArray);
        strcat(dataArray, ",");
        itoa(soundLevel, sensorArray, 10);
        strcat(dataArray, sensorArray);
        strcat(dataArray, 0);
        startTime = millis();
        Serial.println(dataArray);
    }
}


char *dtostrf (double val, signed char width, unsigned char prec, char *sout) {
  char fmt[20];
  sprintf(fmt, "%%%d.%df", width, prec);
  sprintf(sout, fmt, val);
  return sout;
}
